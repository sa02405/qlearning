﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLearningApp
{
    public class QLearning
    {
        private Random _random = new Random();
        private double _gamma;
        public double Gamma { get => _gamma; }

        private double[][] _qTable;
        public double[][] QTable { get => _qTable; }

        private RLSync _qLearningProblem;

        public QLearning(double gamma, RLSync qLearningProblem)
        {
            _qLearningProblem = qLearningProblem;
            _gamma = gamma;
            _qTable = new double[qLearningProblem.NumberOfStates][];
            for (int i = 0; i < qLearningProblem.NumberOfStates; i++)
                _qTable[i] = new double[qLearningProblem.NumberOfActions];
        }

        public void TrainAgent(int numberOfIterations)
        {
            for (int i = 0; i < numberOfIterations; i++)
            {
                int initialState = SetInitialState(_qLearningProblem.NumberOfStates);
                InitializeEpisode(initialState);
            }
        }

        public QLearningStats Run(int initialState)
        {
            if (initialState < 0 || initialState > _qLearningProblem.NumberOfStates) throw new ArgumentException($"The initial state can be between [0-{_qLearningProblem.NumberOfStates}", nameof(initialState));

            var result = new QLearningStats();
            result.InitialState = initialState;
            int state = initialState;
            List<int> actions = new List<int>();
            while (true)
            {
                result.Steps += 1;
                int action = _qTable[state].ToList().IndexOf(_qTable[state].Max());
                state = action;
                actions.Add(action);
                if (_qLearningProblem.GoalStateIsReached(action))
                {
                    result.EndState = action;
                    break;
                }
            }
            result.Actions = actions.ToArray();
            return result;
        }

        private void InitializeEpisode(int initialState)
        {
            //int currentStateIndex = _qLearningProblem.fps_spaceIndex(initialState);
            double currentState = initialState;
            int counter = 0;
            while (true)
            { 
                currentState = TakeAction(currentState);
                counter++;
                if (counter == 100)
                    break;
            }
        }

        private double TakeAction(double currentState)
        {
            //var validActions = _qLearningProblem.GetValidActions(currentState);
            int randomIndexAction = _random.Next(0, _qLearningProblem.NumberOfActions);//choosing a random action from number of actions defined
            double gamma_action = _qLearningProblem.gamma_space[randomIndexAction];//retrieving gamma value form randomIndexAction
            double delta_action = 20; //fixed for now, can be changed in the future
            int delta_actionIndex = 0;

            double saReward = _qLearningProblem.GetReward(currentState, gamma_action, delta_action);
            double nextState = _qLearningProblem.SimulatedFPSValues(randomIndexAction,delta_actionIndex); // FPS for time t+1, delta_actionIndex changes to 4 in the RLSync as it is hardcoded
            double nextStatefps_space = _qLearningProblem.fps_space.OrderBy(x => Math.Abs((long)x - nextState)).First();//need to test first
            int nextStateIndex = _qLearningProblem.fps_spaceIndex(nextStatefps_space);
            double nsReward = _qTable[nextStateIndex].Max();
            double qCurrentState = saReward + (_gamma * nsReward);
            _qTable[currentStateIndex][randomIndexAction] = qCurrentState;
        return nextState;
        }

        private int SetInitialState(int numberOfStates)
        {
            return _random.Next(0, numberOfStates);
        }
    }
}

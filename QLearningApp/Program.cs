﻿using QLearningApp.Common;
using QLearningApp.MachineLearning;
using System;

namespace QLearningApp
{
    public class Program
    {
        
        static void Main()
        {
            Console.WriteLine("step 1");
            /* Setting the parameters - To set the target FPS */
            double targetFPS = 0;
            Console.WriteLine("Enter target FPS value");
            targetFPS= Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("targetFPS from main="+targetFPS);
          
            var qLearning = new QLearning(0.8, new RLSync(),targetFPS);
            Console.WriteLine("Training Agent...");
            qLearning.TrainAgent(3000);

            Console.WriteLine("step 2");
            Console.WriteLine("Training is Done!");
            Console.WriteLine("Press any key to continue...");
            Console.WriteLine("step 3");
            Console.ReadLine();
            do
            {
                double[,] Qdisplay = new double[6, 6];

                Console.WriteLine($"Enter initial state Index. Number between 0 and the number of states (5). Press 'q' to exit...");
                int initialStateIndex = 0; 
                if (!int.TryParse(Console.ReadLine(), out initialStateIndex)) break;

                try
                {
                    var qLearningStats = qLearning.Run(initialStateIndex);
                    Console.WriteLine(qLearningStats.ToString());
                    /* Display 3 dimesional Q matrix */

                    for (int k = 0; k < qLearning.QTable.GetLength(0); k++)
                    {
                        for (int i = 0; i < qLearning.QTable.GetLength(1); i++)
                        {
                            for (int j = 0; j < qLearning.QTable.GetLength(2); j++)
                            {
                                Qdisplay[i, j] = qLearning.QTable[k, i, j];
                                //Console.Write(Math.Round(Qdisplay[i, j] * 10) / 10 + "\t");
                            }
                        }
                        /* To print normalized matrix */
                        var normalizedMatrix = NormalizeMatrix(Qdisplay);
                        for (int i = 0; i < Qdisplay.GetLength(0); i++)
                        {
                            for (int j = 0; j < Qdisplay.GetLength(1); j++)
                            {
                                
                                Console.Write(Math.Round(normalizedMatrix[i, j] * 10) / 10 + "\t");
                            }
                            Console.WriteLine();
                        }
                        Console.WriteLine();
                    }
                    
                    //Console.Write(Qdisplay.ToString());
                    //var normalizedMatrix = NormalizeMatrix(Qdisplay);
                    //Console.Write(qLearning.QTable.ToString());
                    //var normalizedMatrix = qLearning.QTable.NormalizeMatrix();
                    //Console.Write(qLearning.QTable.ToString());
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error: {ex.Message}");
                }
            }
            while (true);
        }
        public static double[,] NormalizeMatrix(double[,] matrix)
        {
            /* To normalize matrix*/
            var maxElement = GetMaxElement(matrix);

            var normalizedMatrix = new double[6,6];
            for (int i = 0; i < matrix.GetLength(0); i++)//0-rows, 1-col,2-z values
            {
                //normalizedMatrix[i] = new double[matrix.Length(0)];
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j] != 0)
                        normalizedMatrix[i,j] = Math.Round((matrix[i, j] * 100) / maxElement, 0);
                }
            }
            return normalizedMatrix;
        }

        public static double GetMaxElement(double[,] matrix)
        {
            double maxElement = 0;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j] > maxElement)
                        maxElement = matrix[i,j];
                }
            }
            return maxElement;
        }
    }
}

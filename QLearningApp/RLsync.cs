﻿using QLearningApp.MachineLearning;
using System;
using System.Collections.Generic;

namespace QLearningApp
{
    public class RLSync
    {
        private Random _random = new Random();
        // Actions
        public double[] gamma_space = { 10, 20, 30, 40, 50, 100 };//{ 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};// the possible values of gamma
        public double[] delta_space = { 10, 20, 30, 40, 50, 100 }; // possible values of delta- 6 values
        //public double[] delta_space = { 20 };

        public double alpha = 0.2; //can control PE with this

        public double beta = 0.2;//can control APL with this
        // State space

        public double[] fps_space = { 10, 12, 15, 17, 20, 22, 25, 27, 30 };//PE, APL 0-30 read it form file-9 states
 
        private double[][] rewards = new double[5][];
        public double Target_fps { get; set;}//made it a proerty 
        /*
         target for syn window if it is 100ms, target should be given by user not hard-coded - done
         ini satse: 20 FPS, 100ms latency 
         initial values 30FPS, APL 100ms--> Target APL 20ms, FPS >=15 
         FPS, PE: 30FPS, PE: 25ms --> target 
         trade -off
         */

        public int NumberOfStates => fps_space.Length;//9

        //public int NumberOfActions => gamma_space.Length * delta_space.Length;//6*6=36
        public int gamma_length => gamma_space.Length;//6
        public int delta_length => delta_space.Length;//6

        public double GetReward(double currentState, double action_gamma, double action_delta)
        {
            //double reward = 1 / (System.Math.Abs(currentState - target_fps)) - alpha * action_gamma - beta * action_delta;
            //double reward = 1 / (1+System.Math.Abs(currentState - target_fps)) - alpha * action_gamma/100 - beta * action_delta/100; // gamma nand delta are normalized to [0, 1]; does not perform well when fps is very low
            double reward = 1 / (1 + System.Math.Abs(currentState - Target_fps)) * (1 - alpha * action_gamma / 100 - beta * action_delta / 100); // gamma nand delta are normalized to [0, 1], so that there is always a positive reward
            return reward;
        }

        public double SimulatedFPSValues(int gammaIndex, int deltaIndex) //these values are for trial and this is the simulated environment
        {

            //keeping deltaIndex as 4 so that RL algo could get the FPS 30 value, changed it to 2 to get the better Q values
            //deltaIndex = _random.Next(3,5);//trying randomness
            double random_fps_noise = 3*(_random.NextDouble()-0.5); // adding uniform random noise to observed fps value
            // deltaIndex = 3;
            double[,] FPS = new double[,] { { 0, 8, 10, 12, 13, 15 },
                                            { 8, 10, 13, 15, 18, 20 },
                                            { 10, 12, 15, 20, 22, 25 },
                                            { 10, 15, 18, 20, 25, 28 }, 
                                            { 10, 15, 20, 22, 30, 30 }, 
                                            { 15, 20, 25, 27, 30, 30 } }; //{ 10, 15, 20, 22, 30, 30 }-second last row
            // 6*6 matrix Further improvements in the simulation: depending on changing network delays, the FPS matrix can change, allowing higher FPS at smaller values of gamma and delta
            return System.Math.Abs(FPS[gammaIndex,deltaIndex] + random_fps_noise);
        }

        public int fps_spaceIndex(double FPS)

        {
            var fps_Index=0;
            for (int i = 0; i<fps_space.Length; i++)
            {
                if (FPS == fps_space[i])
                    fps_Index = i;
                    //break;
            }
            return fps_Index;
        }

        public bool GoalStateIsReached(double CurrentState)
        {
           // Console.WriteLine("target FPS="+target_fps);
            return CurrentState >= Target_fps;
        }
            

    }
    
    
}
    
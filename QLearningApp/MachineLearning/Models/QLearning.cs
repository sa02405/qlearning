﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QLearningApp.MachineLearning
{
    public class QLearning
    {
        private Random _random = new Random();
        private double _gamma;
        public double Gamma { get => _gamma; }

        private double[,,] _qTable;
        public double[,,] QTable { get => _qTable; }

        private RLSync _qLearningProblem;
        List<double> matrixList = new List<double>();

        public QLearning(double gamma, RLSync qLearningProblem, double TargetFPS)
        {
            _qLearningProblem = qLearningProblem;
            _gamma = gamma; // discount factor 
            _qTable = new double[qLearningProblem.NumberOfStates,qLearningProblem.gamma_length,qLearningProblem.delta_length];
            /* Need to convert it in the 3-D array */
            /*for (int i = 0; i < qLearningProblem.NumberOfStates; i++) //fps_space.Length;//9
                _qTable[i] = new double[qLearningProblem.NumberOfActions];*/ //6*6=36
            for (int i = 0; i < qLearningProblem.NumberOfStates; i++)
            {
                for (int j = 0; j < qLearningProblem.gamma_length; j++)
                {
                    for (int k = 0; k < qLearningProblem.delta_length; k++)
                        _qTable[i,j,k] = 0;
                }
            }
            qLearningProblem.Target_fps = TargetFPS;
            /*I can add PE and APL here*/
            Console.WriteLine("In QLearning, qLearningProblem.Target_fps = TargetFPS"+TargetFPS);

        }

        public void TrainAgent(int numberOfIterations)
        {
            for (int i = 0; i < numberOfIterations; i++)
            {
                double initialState = SetInitialState(_qLearningProblem.NumberOfStates);// Argument: number of FPSs e.g. 6 FPS values
                InitializeEpisode(initialState);
            }
        }

        public QLearningStats Run(int initialStateIndex)//returning an object
        {
            if (initialStateIndex < 0 || initialStateIndex > _qLearningProblem.NumberOfStates) throw new ArgumentException($"The initial state can be between [0-{_qLearningProblem.NumberOfStates}", nameof(initialStateIndex));
            var result = new QLearningStats();
            result.InitialState = _qLearningProblem.fps_space[initialStateIndex];
            int stateIndex = initialStateIndex;
            int jmax = 0, kmax = 0;
            List<double> gammaactions = new List<double>();
            List<double> deltaactions = new List<double>();
            List<double> stateFPS = new List<double>();
            double prev_state = result.InitialState;
            while (true)
            {
                result.Steps += 1;
                //int actionIndex = _qTable[stateIndex].ToList().IndexOf(_qTable[stateIndex].Max());
                //state = action;
                //int actionIndex = matrixList.IndexOf(matrixList.Max());
                
                //int actionIndex= _qTabl[0,0].ToList().IndexOf(_qTable[stateIndex,0,0].Max());//it would be nice if it works
                double element = _qTable[stateIndex, 0, 0];
                for (int j = 0; j < _qLearningProblem.delta_length; j++)
                        for (int k = 0; k < _qLearningProblem.gamma_length; k++)
                        {
                            if (_qTable[stateIndex, j, k] >= element)
                            {
                                element = _qTable[stateIndex, j, k];
                                jmax = j;//gamma values
                                kmax = k;//delta values
                            }
                        }
                
                double state = _qLearningProblem.SimulatedFPSValues(jmax,kmax);//FPS value

                //Console.WriteLine("Next state FPS"+state);
                double statefps_space = _qLearningProblem.fps_space.OrderBy(x => Math.Abs((long)x - state)).First();
                stateIndex = _qLearningProblem.fps_spaceIndex(statefps_space);
                double gamma_action = _qLearningProblem.gamma_space[jmax];
                double delta_action = _qLearningProblem.delta_space[kmax];
               
                //Console.WriteLine("Current FPS state" + prev_state);
                Console.WriteLine("Current FPS state: " + prev_state + ", Gamma Action: " + gamma_action + ", delta Action: " + delta_action + ", Next FPS state: " + state);
                prev_state = state;
                gammaactions.Add(gamma_action);
                deltaactions.Add(delta_action);
                stateFPS.Add(state);
                /* Newly added code to make it finite loop */
                if (_qLearningProblem.GoalStateIsReached(state))
                {
                    result.EndState = state;
                    //need to write code here to end a timer
                    break;
                }
                /*if (counter == 50)
                { break; }*/
            }
            result.gammaActions = gammaactions.ToArray();
            result.deltaActions = deltaactions.ToArray();
            return result;
        }

        private void InitializeEpisode(double initialState)
        {
            //int currentStateIndex = _qLearningProblem.fps_spaceIndex(initialState);

            //Console.WriteLine("Enter targetted FPS value ={0}",targetFPS);
            double currentState = initialState;//FPS value
            int counter = 0;
            while (true)
            {
                currentState = TakeAction(currentState);//currentState= FPS, return should be FPS value
                counter++;
                if (counter == 2000)
                    break;//will run 500 times*/
                /*if (_qLearningProblem.GoalStateIsReached(currentState))
                    break;*/
            }
        }

        private double TakeAction(double currentState)
        {
            //var validActions = _qLearningProblem.GetValidActions(currentState);
            int randomIndexActionGamma = _random.Next(0, _qLearningProblem.gamma_length);
            int randomIndexActionDelta = _random.Next(0, _qLearningProblem.delta_length);
            //Console.WriteLine("randomIndexActionGamma ={0},randomIndexActionDelta ={1}", randomIndexActionGamma, randomIndexActionDelta);
            //int randomIndexActionDelta = _random.Next(0, _qLearningProblem.NumberOfActions);//choosing a random action from number of actions defined
            double gamma_action = _qLearningProblem.gamma_space[randomIndexActionGamma];//retrieving gamma value form randomIndexAction
                                                                                    // double delta_action = _qLearningProblem.delta_space[randomIndexActionGamma];
            double delta_action = _qLearningProblem.gamma_space[randomIndexActionDelta]; //fixed for now, can be changed in the future
            //int delta_actionIndex = 0;
            double rho = 0.1;//0.4;

            double saReward = _qLearningProblem.GetReward(currentState, gamma_action, delta_action);
            // Use rho to have a slowly & smoothly changing FPS value after an action is taken
            double nextState = rho * currentState + (1 - rho) * _qLearningProblem.SimulatedFPSValues(randomIndexActionGamma, randomIndexActionDelta); // FPS for time t+1, delta_actionIndex changes to 4 in the RLSync as it is hardcoded
            double nextStatefps_space = _qLearningProblem.fps_space.OrderBy(x => Math.Abs((long)x - nextState)).First();//need to test first, looking for the closet value of the FPS
            double nextStateIndex = _qLearningProblem.fps_spaceIndex(nextStatefps_space);
            //double nsReward = _qTable[Convert.ToInt32(nextStateIndex), 0, 0];
            /* 
             Next state index is fixed and need to find out the highest value of Q based on it)
             */
            double Max = _qTable[Convert.ToInt32(nextStateIndex), 0, 0];
            for (int j = 0; j < _qLearningProblem.delta_length; j++)
                for (int k = 0; k < _qLearningProblem.gamma_length; k++)
                {
                    if (_qTable[Convert.ToInt32(nextStateIndex), j, k] > Max)
                    {
                        Max = _qTable[Convert.ToInt32(nextStateIndex), j, k];
                    }
                }
            
            double nsReward = Max;
            //Console.WriteLine("value of nsReward=" + nsReward);
            double qCurrentState = saReward + (_gamma * nsReward);//discount factor=0.8
            int currentStateIndex = _qLearningProblem.fps_spaceIndex(currentState);
            _qTable[currentStateIndex, randomIndexActionGamma, randomIndexActionDelta] = qCurrentState;
            return nextState;
        }

        private double SetInitialState(int numberOfStates)
        {
            int stateIndex = _random.Next(0, numberOfStates);
            double initialState = _qLearningProblem.fps_space[stateIndex];
            return initialState;

        }

    }
}


/*
 for (int i = 0; i < _qLearningProblem.NumberOfStates; i++)//check here
                    for (int j = 0; j < _qLearningProblem.delta_length; j++)
                        for (int k = 0; k < _qLearningProblem.gamma_length; k++)
                        {
                            matrixList.Add(_qTable[i, j, k]);
                        }
 
 */
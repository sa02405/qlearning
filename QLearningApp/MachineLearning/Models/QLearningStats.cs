﻿using System.Text;

namespace QLearningApp.MachineLearning
{
    public class QLearningStats
    {
        public double InitialState { get; set; }
        public double EndState { get; set; }
        public int Steps { get; set; }
        public double[] gammaActions { get; set; }
        public double[] deltaActions { get; set; }

        public double[] State { get; set; }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Agent needed {Steps} steps to find the solution");
            sb.AppendLine($"Agent Initial State: {InitialState}");
            foreach (var action in gammaActions)
                sb.AppendLine($"gammaAction: {action}");
            foreach (var action in deltaActions)
                sb.AppendLine($"deltaAction: {action}");
            sb.AppendLine($"Agent arrived at the goal state: {EndState}");
            return sb.ToString();
        }
    }
}
